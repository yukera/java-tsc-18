package com.tsc.jarinchekhina.tm.command;

import com.tsc.jarinchekhina.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand{

    public void print(Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
    }

}
