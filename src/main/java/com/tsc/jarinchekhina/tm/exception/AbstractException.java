package com.tsc.jarinchekhina.tm.exception;

public abstract class AbstractException extends Exception {

    public AbstractException(final Throwable cause) {
        super(cause);
    }

    public AbstractException(final String error) {
        super(error);
    }

}
