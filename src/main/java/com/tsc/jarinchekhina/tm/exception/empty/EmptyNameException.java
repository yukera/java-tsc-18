package com.tsc.jarinchekhina.tm.exception.empty;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
