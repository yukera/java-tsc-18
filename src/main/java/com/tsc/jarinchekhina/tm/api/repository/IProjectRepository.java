package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

}
