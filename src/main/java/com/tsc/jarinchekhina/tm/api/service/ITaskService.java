package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void add(Task task);

    void remove(Task task);

    void clear();

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task create(String name) throws EmptyNameException;

    Task create(String name, String description) throws AbstractException;

    Task findOneById(String id) throws EmptyIdException;

    Task findOneByIndex(Integer index) throws EmptyIdException;

    Task findOneByName(String name) throws EmptyNameException;

    Task removeOneById(String id) throws EmptyIdException;

    Task removeOneByIndex(Integer index) throws EmptyIdException;

    Task removeOneByName(String name) throws EmptyNameException;

    Task updateTaskById(String id, String name, String description) throws AbstractException;

    Task updateTaskByIndex(Integer index, String name, String description) throws AbstractException;

    Task startTaskById(String id) throws AbstractException;

    Task startTaskByIndex(Integer index) throws AbstractException;

    Task startTaskByName(String name) throws AbstractException;

    Task finishTaskById(String id) throws AbstractException;

    Task finishTaskByIndex(Integer index) throws AbstractException;

    Task finishTaskByName(String name) throws AbstractException;

    Task changeTaskStatusById(String id, Status status) throws AbstractException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractException;

    Task changeTaskStatusByName(String name, Status status) throws AbstractException;

}
