package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    void clearProjects();

    List<Task> findAllTaskByProjectId(String projectId) throws EmptyIdException;

    Task bindTaskByProjectId(String projectId, String taskId) throws AbstractException;

    Task unbindTaskByProjectId(String taskId) throws AbstractException;

    Project removeProjectById(String projectId) throws EmptyIdException;

}
